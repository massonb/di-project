import models.DV_models as models


def DVModel(config):
    def build_DV( input_shape, config):
        #randN_05 = torch.normal(2, 3, size=input_shape)
        #bias_init = randN_05
        model = models.Model(config)
        list = model.initialization(input_shape)
        
       
        
        return model

    y_in_shape = [config.batch_size, config.bptt, config.batch_size] #config.x_dim * (1 + config.contrastive_duplicates)]
    xy_in_shape = [config.batch_size, config.bptt, config.batch_size] #config.x_dim + config.y_dim) * (1 + config.contrastive_duplicates)]

    h_y_model = build_DV(y_in_shape,
                         config)
    h_xy_model = build_DV(xy_in_shape,
                          config)

    model = {'y': h_y_model,
             'xy': h_xy_model}

    return model

def DVModelMINE(config):
    def build_DV(input_shape, config):
        
        model = models.Model_simp(config)
        t = model.initialization(input_shape)
        return model

    in_shape = [config.batch_size, config.x_dim + config.y_dim]

    model = build_DV(in_shape, config)

    return model
