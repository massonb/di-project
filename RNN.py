import torch
import random

class RNNModel(torch.nn.Module):
    def __init__(self, input_size, HL_size, output_size):
        super(RNNModel, self).__init__()
        self.rnn = torch.nn.RNN(input_size=input_size,
                                hidden_size= HL_size,
                                num_layers=1,
                                bidirectional=False)
        self.hidden_dim = HL_size
        self.input_size = input_size
        self.fc = torch.nn.Linear(HL_size, output_size)
    


    def forward(self, input, hidden = None):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        batch_size = input.size(-1)
        #hidden = self.init_hidden(batch_size)
        #hidden = hidden.to(device)
        out, hidden = self.rnn(input, hidden)
        out = out.contiguous().view(-1, self.hidden_dim)
        out = self.fc(out)
        
        return out, hidden
    
    def init_hidden(self, batch_size):
        # This method generates the first hidden state of zeros which we'll use in the forward pass
        # We'll send the tensor holding the hidden state to the device we specified earlier as well
        hidden = torch.zeros(batch_size, self.hidden_dim)
        return hidden

class DILoss(torch.nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(DILoss, self).__init__()


    def forward(self, outputs, outputs_tilde):
        size = outputs.size()[0]
        value = 1/size * torch.sum(outputs)
        value_log = torch.log(1/size * torch.sum(torch.exp(outputs_tilde)))
        return value - value_log


def DIloss(outputs, outputs_tilde):
    size = outputs.size()[0]
    value = 1/size * torch.sum(outputs)
    value_log = torch.log(1/size * torch.sum(torch.exp(outputs_tilde)))
    return value - value_log

def train_DINE(model_y: RNNModel, model_xy: RNNModel, data_y, data_xy, config, loss_fn):
    train_losses = {}
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model_y.to(device)
    model_xy.to(device)
    optimizer_y = torch.optim.SGD(model_y.parameters(), lr = 0.00015, maximize=True)
    optimizer_xy = torch.optim.SGD(model_xy.parameters(), lr = 0.00015, maximize=True)
    # training : step 1
    for epoch in range(config.num_epochs):
        epoch_losses = list()

        X_y = data_y[0].to(device)
        X_xy = data_xy[0].to(device)
            
        optimizer_y.zero_grad()
        optimizer_xy.zero_grad()

        loss_y = 0
        loss_xy = 0
        for c in range(X_y.shape[1]):
                
            out_y, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0],config.x_dim))
            
            out_xy, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0],config.x_dim+config.y_dim))
            

            out_y_tilde, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0],config.x_dim))
            out_xy_tilde, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0],config.x_dim+config.y_dim))

            # out_y, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0]))
            # out_xy, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0]))
            # out_y_tilde, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0]))
            # out_xy_tilde, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0]))

            #out_y_tilde, hidden_y_tilde = model_y(Y_y[:, c, :4].reshape(Y_y.shape[0],config.x_dim))
            #out_xy_tilde, hidden_xy_tilde = model_xy(Y_xy[:, c,:8].reshape(Y_xy.shape[0],config.x_dim+config.y_dim))

            l_y = loss_fn(out_y, out_y_tilde)
            l_xy = loss_fn(out_xy, out_xy_tilde)
                
            loss_y += l_y
            loss_xy += l_xy


        loss_y.backward()
        loss_xy.backward()
        optimizer_y.step()
        optimizer_xy.step()
            
        epoch_losses.append(loss_xy.detach().item()-loss_y.detach().item())
        train_losses[epoch] = torch.tensor(epoch_losses).mean()
        print(f'=> epoch: {epoch + 1}, loss: {train_losses[epoch]}')
    return model_y, model_xy, train_losses
    
    
    # step 2
def evaluation(model_y: RNNModel, model_xy: RNNModel, data_y, data_xy, config, loss_fn : torch.nn.modules):
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model_y.to(device)
    model_xy.to(device)
    
    X_y, Y_y = data_y[0].to(device), data_y[1].to(device)
    X_xy, Y_xy = data_xy[0].to(device), data_xy[1].to(device)
    

    loss_y = 0
    loss_xy = 0
    for c in range(X_y.shape[1]):
        out_y, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0],config.x_dim))
        out_xy, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0],config.x_dim+config.y_dim))
        

        out_y_tilde, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0],config.x_dim))
        out_xy_tilde, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0],config.x_dim+config.y_dim))

        # out_xy, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0]))
        # out_y, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0]))
        # out_y_tilde, hidden_y = model_y(X_y[:,c].reshape(X_y.shape[0]))
        # out_xy_tilde, hidden_xy = model_xy(X_xy[:,c].reshape(X_xy.shape[0]))


        l_y = loss_fn(out_y, out_y_tilde)
        l_xy = loss_fn(out_xy, out_xy_tilde)
                
        loss_y += l_y
        loss_xy += l_xy
    return loss_xy-loss_y