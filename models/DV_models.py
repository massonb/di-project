import torch.nn as nn
import torch
from models.layers import ModLSTM

class LambdaLayer(nn.Module):
    def __init__(self, lambd, size):
        super(LambdaLayer, self).__init__()
        self.lambd = lambd
        self.size = size
    def forward(self, x):
        return self.lambd(x, 100, 2)

class Model(torch.nn.Module):
    def __init__(self, config):
        super(Model, self).__init__()
        self.lstm = ModLSTM(config.DI_hidden,
                        dropout=config.DI_dropout, recurrent_dropout=config.DI_dropout,
                        contrastive_duplicates=config.contrastive_duplicates)
        self.split = LambdaLayer(torch.split, config.contrastive_duplicates)
        #dense0 = nn.Linear(config.DI_hidden, bias_init)
        #dense1 = nn.Linear(config.DI_last_hidden, bias_init)
        #dense2 = nn.Linear(1, bias_init)
        self.dense0 = nn.Linear(config.DI_hidden, config.DI_hidden)
        self.dense1 = nn.Linear(config.DI_hidden, config.DI_hidden)
        self.dense2 = nn.Linear(config.DI_hidden, 1)



    def forward(self, input):
        t, h = self.lstm(input)
        t_1= self.split(t)

        t_1, t_2 = self.dense0(t_1[0]), self.dense0(t_1[1])
        t_1, t_2 = self.dense1(t_1), self.dense1(t_2)
        t_1, t_2 = self.dense2(t_1), torch.exp(self.dense2(t_2)).cuda()

        return [t_1, t_2]


    def initialization(self, input_shape):
        in_ = t = torch.randn(input_shape)

        t, h = self.lstm(t)
        t_1= self.split(t)

        t_1, t_2 = self.dense0(t_1[0]), self.dense0(t_1[1])
        t_1, t_2 = self.dense1(t_1), self.dense1(t_2)
        t_1, t_2 = self.dense2(t_1), torch.exp(self.dense2(t_2)).cuda()

        return [t_1, t_2]
    

class Model_simp(torch.nn.Module):
    def __init__(self, config):
        super(Model_simp, self).__init__()
        self.dense0 = nn.Linear(config.DI_hidden, config.DI_hidden)
        self.dense1 = nn.Linear(config.DI_hidden, config.DI_hidden)
        self.dense2 = nn.Linear(config.DI_hidden, 1)
    
    def forward(self, input):
        t = self.dense0(t)
        t = self.dense1(t)
        t = self.dense2(t)

        return [t]
        
    def initialization(self, input_shape):
        
        t = torch.randn(input_shape)

        t = self.forward(t)

        return [t]