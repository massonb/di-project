import torch
import RNN as rnn
import tensorflow as tf
import torch
import transform as tr
import math


def DINE(data, config, y_feedback, model):
    # Initialisation
    rnn_theta_y = rnn.RNNModel(config.x_dim, 100, 1)    
    rnn_theta_xy = rnn.RNNModel(config.x_dim+config.y_dim, 100, 1)
    
    n = 0
    list = []
    I_di = []
    save_y = []
    save_xy = []
    for sample in data["train"]():
    #test = torch.Tensor(sample.numpy())

        X_batch = [sample, y_feedback]
        [x, y] = model['ndt'](X_batch)  # obtain samples through model
        [input_y, input_xy] = tr.DI_data(x, y, config)
    
        input_y_tilde = input_y[1]
        input_y = input_y[0]

        input_y = torch.stack(tr.converter(input_y))
        input_y_tilde = torch.stack(tr.converter(input_y_tilde))


        input_xy_tilde = input_xy[1]
        input_xy = input_xy[0]

        input_xy = torch.stack(tr.converter(input_xy))
        input_xy_tilde = torch.stack(tr.converter(input_xy_tilde))
    
        data_y = [input_y, input_y_tilde]
        data_xy = [input_xy, input_xy_tilde]
        save_y.append(data_y)
        save_xy.append(data_xy)
        # step 1
        rnn_theta_y, rnn_theta_xy, train_loss = rnn.train_DINE(rnn_theta_y, rnn_theta_xy, data_y, data_xy, config, rnn.DIloss)


    

        

    for sample in data["eval"]():
    #test = torch.Tensor(sample.numpy())

        X_batch = [sample, y_feedback]
        [x, y] = model['ndt'](X_batch)  # obtain samples through model
        [input_y, input_xy] = tr.DI_data(x, y, config)
    
        input_y_tilde = input_y[1]
        input_y = input_y[0]

        input_y = torch.stack(tr.converter(input_y))
        input_y_tilde = torch.stack(tr.converter(input_y_tilde))


        input_xy_tilde = input_xy[1]
        input_xy = input_xy[0]

        input_xy = torch.stack(tr.converter(input_xy))
        input_xy_tilde = torch.stack(tr.converter(input_xy_tilde))
    
        data_y = [input_y, input_y_tilde]
        data_xy = [input_xy, input_xy_tilde]
        save_y.append(data_y)
        save_xy.append(data_xy)

    list = []
    I_di = []
    I_di_nats = []
    for k in range(len(save_y)):
        list.append(k+1)
        value =rnn.evaluation(rnn_theta_y, rnn_theta_xy, save_y[k], save_xy[k], config, rnn.DIloss).to("cpu").detach().numpy()
        I_di.append(value)
        I_di_nats.append(-abs(value)*math.log(abs(value)))
    return list, I_di, I_di_nats, rnn_theta_y, rnn_theta_xy, train_loss

    