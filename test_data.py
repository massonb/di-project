
from data.data_loader import load_data
from models.models import build_model
from utils.utils import preprocess_meta_data
import tensorflow as tf
import numpy as np
import DINE
import matplotlib.pylab as mp
import math

def save_dataset(list, name):
    file = open(name, 'w')
    for k in range(len(list)):
        row = ""
        for i in range(len(list[k])):
            row = row + str(list[k][i])+";"
        row = row[:-1] + "\n"
        file.write(row)
    file.close()

config = preprocess_meta_data()
model = build_model(config)

if config.feedback:
    y_feedback = tf.convert_to_tensor(np.zeros([config.batch_size, 1, 2*config.x_dim]))
else:
    y_feedback = tf.convert_to_tensor(np.zeros([config.batch_size, 1, config.x_dim]))


data = load_data(config)
list, I_DI, I_di_nats, rnn_y, rnn_xy, train_loss = DINE.DINE(data, config, y_feedback, model)

mp.figure(1)
mp.plot(list, I_DI)
mp.show()

mp.figure(2)
mp.plot(list, I_di_nats)
mp.show()

evolution = []
H = 0
for k in I_DI:
    H += -abs(k)*math.log(abs(k))
    evolution.append(H)

mp.figure(3)
mp.plot(list, evolution)
mp.show()

print(f'Result of the final evaluation, I_DI(Dn): {H}')