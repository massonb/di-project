import tensorflow as tf
import torch

def DI_data(x, y, config):

    if config.contrastive_type == "uniform":
        contrastive_fn = contrastive_uniform
    elif config.contrastive_type == "gauss":
        contrastive_fn = contrastive_gauss
    else:
        raise ValueError("invalid contrastive noise type")


    y_tilde = []
    xy_tilde = []
    for i in range(config.contrastive_duplicates):
        y_tilde.append(contrastive_fn(y))
        xy_tilde.append(tf.concat([x, y_tilde[i]], axis=-1))
    y_tilde = tf.concat(y_tilde, axis=-1)
    input_y = [y, y_tilde]
    #input_y = tf.concat([y, y_tilde], axis=-1)
    xy = tf.concat([x, y], axis=-1)
    xy_tilde = tf.concat(xy_tilde, axis=-1)
    input_xy = [xy, xy_tilde]
    #input_xy = tf.concat([xy, xy_tilde], axis=-1)
    return [input_y, input_xy]

def contrastive_uniform(y):
    return tf.random.uniform(tf.shape(y), minval=tf.reduce_min(y), maxval=tf.reduce_max(y))

def contrastive_gauss(y):
    std = tf.math.reduce_std(y)
    return tf.random.normal(shape=tf.shape(y), dtype=tf.float64, stddev=std)

def converter(list):
    out = []
    for sample in list:
        out.append(torch.Tensor(sample.numpy()))
    return out
