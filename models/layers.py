

import torch
import torch.nn as nn
from torch.nn import LSTMCell
############ Modified LSTM Layers ####################
class LSTMCellNew(LSTMCell):
    """
    Modified implementation of the LSTM cell such that It has 2 inputs: (y, y_bar).
    The recurrent state (h) is calculated for each input, but the input state for both states is only h(y)
    output is a pair of states sequences (h(y),h_bar(y))
    """

    # Create a new build function to suite our needs:
    def __init__(self,
                 units,
                 use_bias=True,
                 unit_forget_bias=True,
                 dropout=0.,
                 recurrent_dropout=0.,
                 implementation=1,
                 contrastive_duplicates=1,
                 **kwargs):
        super(LSTMCellNew, self).__init__(units, units, **kwargs)
        self.units = units
        self.hidden_size = units
        #self.activation = activations.get(activation)
        self.activation = nn.Tanh
        self.recurrent_activation = nn.Hardsigmoid
        self.use_bias = use_bias

        self.kernel_initializer = nn.init.xavier_uniform_
        self.recurrent_initializer = nn.init.orthogonal_
        self.bias_initializer = nn.init.zeros_
        self.unit_forget_bias = unit_forget_bias



        self.dropout = min(1., max(0., dropout))
        self.recurrent_dropout = min(1., max(0., recurrent_dropout))
        self.implementation = implementation
        # tuple(_ListWrapper) was silently dropping list content in at least 2.7.10,
        # and fixed after 2.7.16. Converting the state_size to wrapper around
        # NoDependency(), so that the base_layer.__setattr__ will not convert it to
        # ListWrapper. Down the stream, self.states will be a list since it is
        # generated from nest.map_structure with list, and tuple(list) will work
        # properly.
        self.state_size = [self.units, self.units]
        self.output_size = self.units

        self.contrastive_duplicates = contrastive_duplicates

    def build(self, input_shape):
        input_dim = input_shape[-1]
        self.kernel = self.add_weights(
            [input_dim // (self.contrastive_duplicates+1), self.units * 4])
        self.recurrent_kernel = self.add_weights(
            [self.units, self.units * 4])

        if self.use_bias:
            if self.unit_forget_bias:
                def bias_initializer(_, *args, **kwargs):
                    return torch.concatenate([self.bias_initializer((self.units,), *args, **kwargs),
                                          nn.init.ones_()((self.units,), *args, **kwargs),
                                          self.bias_initializer((self.units * 2,), *args, **kwargs),
                                          ]).cuda()
            else:
                bias_initializer = self.bias_initializer

            self.bias = self.add_bias([self.units * 4,])
        else:
            self.bias = None
        self.built = True

    def _compute_carry_and_output(self, x, h_tm1, c_tm1):
        """Computes carry and output using split kernels."""
        x_i, x_f, x_c, x_o = x
        h_tm1_i, h_tm1_f, h_tm1_c, h_tm1_o = h_tm1
        i = self.recurrent_activation(
            x_i + torch.dot(h_tm1_i, self.recurrent_kernel[:, :self.units]).cuda())
        f = self.recurrent_activation(x_f + torch.dot(
            h_tm1_f, self.recurrent_kernel[:, self.units:self.units * 2]).cuda())
        c = f * c_tm1 + i * self.activation(x_c + torch.dot(
            h_tm1_c, self.recurrent_kernel[:, self.units * 2:self.units * 3]).cuda())
        o = self.recurrent_activation(
            x_o + torch.dot(h_tm1_o, self.recurrent_kernel[:, self.units * 3:]).cuda())
        return c, o

    def _compute_carry_and_output_fused(self, z, c_tm1):
        """Computes carry and output using fused kernels."""
        z0, z1, z2, z3 = z
        i = self.recurrent_activation(z0)
        f = self.recurrent_activation(z1)
        c = f * c_tm1 + i * self.activation(z2)
        o = self.recurrent_activation(z3)
        return c, o

    def call(self, inputs_stacked, states, training=None):
        def cell_(inputs, h_tm1, c_tm1):
            dp_mask = self.get_dropout_mask_for_cell(inputs, training, count=4)
            rec_dp_mask = self.get_recurrent_dropout_mask_for_cell(
                h_tm1, training, count=4)

            if self.implementation == 1:
                if 0 < self.dropout < 1.:
                    inputs_i = inputs * dp_mask[0]
                    inputs_f = inputs * dp_mask[1]
                    inputs_c = inputs * dp_mask[2]
                    inputs_o = inputs * dp_mask[3]
                else:
                    inputs_i = inputs
                    inputs_f = inputs
                    inputs_c = inputs
                    inputs_o = inputs
                k_i, k_f, k_c, k_o = torch.tensor_split.split(self.kernel, num_or_size_splits=4, axis=1)
                x_i = torch.dot(inputs_i, k_i).cuda()
                x_f = torch.dot(inputs_f, k_f).cuda()
                x_c = torch.dot(inputs_c, k_c).cuda()
                x_o = torch.dot(inputs_o, k_o).cuda()
                if self.use_bias:
                    b_i, b_f, b_c, b_o = torch.tensor_split.split(self.bias, num_or_size_splits=4, axis=0)
                    x_i = torch.bias_add(x_i, b_i).cuda()
                    x_f = torch.bias_add(x_f, b_f).cuda()
                    x_c = torch.bias_add(x_c, b_c).cuda()
                    x_o = torch.bias_add(x_o, b_o).cuda()

                if 0 < self.recurrent_dropout < 1.:
                    h_tm1_i = h_tm1 * rec_dp_mask[0]
                    h_tm1_f = h_tm1 * rec_dp_mask[1]
                    h_tm1_c = h_tm1 * rec_dp_mask[2]
                    h_tm1_o = h_tm1 * rec_dp_mask[3]
                else:
                    h_tm1_i = h_tm1
                    h_tm1_f = h_tm1
                    h_tm1_c = h_tm1
                    h_tm1_o = h_tm1
                x = (x_i, x_f, x_c, x_o)
                h_tm1 = (h_tm1_i, h_tm1_f, h_tm1_c, h_tm1_o)
                c, o = self._compute_carry_and_output(x, h_tm1, c_tm1)
            else:
                if 0. < self.dropout < 1.:
                    inputs = inputs * dp_mask[0]
                z = torch.dot(inputs, self.kernel).cuda()
                if 0. < self.recurrent_dropout < 1.:
                    h_tm1 = h_tm1 * rec_dp_mask[0]
                z += torch.dot(h_tm1, self.recurrent_kernel).cuda()
                if self.use_bias:
                    z = self.bias_add(z, self.bias)

                z = torch.tensor_split.split(z, 4, 1)
                c, o = self._compute_carry_and_output_fused(z, c_tm1)

            h = o * self.activation(c)
            return h, c

        h_tm1 = states[0]  # previous memory state
        c_tm1 = states[1]  # previous carry state
        in_ = torch.split(inputs_stacked, 1+self.contrastive_duplicates, -1)
        in_1 = in_[0]
        in_contrastive = in_[1:]
        h_1, c_1 = cell_(in_1, h_tm1, c_tm1)
        contrastive_outs = [cell_(in_c, h_tm1, c_tm1) for in_c in in_contrastive]
        h_contrastive  = [h_ for h_, _ in contrastive_outs]

        h = torch.stack([h_1] + h_contrastive, axis=-2).cuda()
        recurrent_states = [h_1, c_1]
        return h, recurrent_states

    def add_weights(self, shape):
        return nn.init.xavier_normal_(torch.empty(shape))
            
    def add_bias(self, shape):
        return nn.init.zeros_(torch.empty(shape))

    def bias_add(self, z, bias):
        z = torch.tensor(z)
        z.add(bias)
        return z


class ModLSTM(nn.RNN):
    # @interfaces.legacy_recurrent_support
    def __init__(self, units,
                 use_bias=True,
                 unit_forget_bias=False,
                 dropout=0.,
                 recurrent_dropout=0.,
                 implementation=2,
                 contrastive_duplicates=1,
                 **kwargs):
        cell = LSTMCellNew(units,
                           use_bias=use_bias,
                           unit_forget_bias=unit_forget_bias,
                           dropout=dropout,
                           recurrent_dropout=recurrent_dropout,
                           implementation=implementation,
                           contrastive_duplicates=contrastive_duplicates)
        print(cell)
        super(ModLSTM, self).__init__(units, units,
                                      bidirectional = True,
                                      **kwargs)